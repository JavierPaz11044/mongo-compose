#!/bin/bash
until docker exec -it mongodb_container mongo --eval "print(\"Conectado a MongoDB\")"
do
    sleep 1
done

# Ejecuta el comando de restauración
docker exec -it mongodb_container mongorestore --drop --db berry /data/db/test/

echo "Restauración completada"